package main

import (
	"fmt"
	"github.com/joho/godotenv"
	pb "gitlab.com/beearn/datawarehouse/wrpc"
	"gitlab.com/beearn/datawarehouse/wrpc/client"
	"gitlab.com/beearn/mirror/repo"
	"gitlab.com/beearn/mirror/service"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"time"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
	addr := os.Getenv("WAREHOUSE_IP")
	port := os.Getenv("WAREHOUSE_PORT")
	ClAddr := fmt.Sprintf(":%s", os.Getenv("SERVER_PORT"))

	warehouseClient := client.NewWarehouseClient(fmt.Sprintf("%s:%s", addr, port), 360*time.Second)
	repository := repo.NewRepo(*warehouseClient)
	service := service.NewService(*repository)
	lis, err := net.Listen("tcp", ClAddr)
	if err != nil {
		log.Fatal(err)
	}
	s := grpc.NewServer(grpc.MaxRecvMsgSize(1024*1024*1024), grpc.MaxSendMsgSize(1024*1024*1024))
	pb.RegisterWarehouseServer(s, service)

	ticker := time.NewTicker(15 * time.Second)
	defer ticker.Stop()
	go func() {
		fmt.Println("worker started")
		service.GetAll()
		for {
			select {
			case <-ticker.C:
				service.GetAll()
			}
		}
	}()

	if err != nil {
		fmt.Println(err)
	}
	log.Printf("grpc server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatal(err)
	}

}
