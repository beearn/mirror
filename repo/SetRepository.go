package repo

import (
	"context"
	"errors"
	"fmt"
	"gitlab.com/beearn/datawarehouse/wrpc"
	"gitlab.com/beearn/datawarehouse/wrpc/client"
	"gitlab.com/beearn/entity"
	"google.golang.org/protobuf/types/known/emptypb"
	"sync"
)

type Repoer interface {
	GetAllDashboardIn()
	GetPriceChangeIn()
	GetAllDashboard(ctx context.Context, in *emptypb.Empty) (*wrpc.GetAllDashboardOut, error)
	GetDashboardsBySymbols(ctx context.Context, in *wrpc.GetDashboardsBySymbolsIn) (*wrpc.GetDashboardsBySymbolsOut, error)
	GetPriceChangeBySymbol(ctx context.Context, in *wrpc.GetDashboardIn) (*wrpc.PriceChangeStats, error)
	GetPriceChange(ctx context.Context, empty *emptypb.Empty) (*wrpc.GetPriceChangeOut, error)
	GetDashboard(ctx context.Context, in *wrpc.GetDashboardIn) (*wrpc.DashboardPeriods, error)
}

type Repo struct {
	Client client.WarehouseClient
	Cache  map[string]interface{}
	mu     *sync.Mutex
}

func NewRepo(Client client.WarehouseClient) *Repo {
	return &Repo{
		Client: Client,
		Cache:  make(map[string]interface{}),
		mu:     &sync.Mutex{},
	}
}

func (r *Repo) GetAllDashboardIn() {
	r.mu.Lock()
	defer r.mu.Unlock()
	getAll := r.Client.GetAllDashboard()
	fmt.Println("GetAllDashboardIn count", len(getAll))
	r.Cache["GetAll"] = getAll
}

func (r *Repo) GetPriceChangeIn() {
	r.mu.Lock()
	defer r.mu.Unlock()
	Price := r.Client.GetPriceChange()
	fmt.Println("GetPriceChangeIn count", len(Price))
	r.Cache["GetPrice"] = Price
}

func (r *Repo) GetAllDashboard(ctx context.Context, in *emptypb.Empty) (*wrpc.GetAllDashboardOut, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	data, ok := r.Cache["GetAll"]
	if !ok {
		return nil, errors.New("cant get data from GetAll")
	}
	periodsRaw, ok := data.([]*entity.DashboardPeriods)
	if !ok {
		fmt.Println("cant convert to map")
	}
	var periods []*wrpc.DashboardPeriods
	for i := range periodsRaw {
		periods = append(periods, convertDashboardPeriods(periodsRaw[i]))
	}
	response := wrpc.GetAllDashboardOut{
		Dashboard: periods,
	}

	return &response, nil
}

func (r *Repo) GetDashboardsBySymbols(ctx context.Context, in *wrpc.GetDashboardsBySymbolsIn) (*wrpc.GetDashboardsBySymbolsOut, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	data, ok := r.Cache["GetAll"]
	if !ok {
		fmt.Println("cant get data from GetAll")
	}
	periodsRaw, ok := data.([]*entity.DashboardPeriods)
	if !ok {
		fmt.Println("cant convert to map")
	}
	var periods []*wrpc.DashboardPeriods
	for i := range periodsRaw {
		for _, symbol := range in.Symbols {
			if periodsRaw[i].Symbol == symbol {
				periods = append(periods, convertDashboardPeriods(periodsRaw[i]))
			}
		}
	}
	response := wrpc.GetDashboardsBySymbolsOut{
		Dashboard: periods,
	}

	return &response, nil
}

func (r *Repo) GetPriceChangeBySymbol(ctx context.Context, in *wrpc.GetDashboardIn) (*wrpc.PriceChangeStats, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	data, ok := r.Cache["GetPrice"]
	if !ok {
		return nil, errors.New("cant get data from GetPrice")
	}
	sliceData, ok := data.([]*entity.PriceChangeStats)
	if !ok {
		return nil, errors.New("cant convert data in GetPrice")
	}
	for i := range sliceData {
		if sliceData[i].Symbol == in.Symbol {
			return convertPriceChangeStats(sliceData[i]), nil
		}
	}
	return nil, errors.New("cant find price change")
}

func (r *Repo) GetPriceChange(ctx context.Context, empty *emptypb.Empty) (*wrpc.GetPriceChangeOut, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	data, ok := r.Cache["GetPrice"]
	if !ok {
		return nil, errors.New("cant get data from GetPriceChange")
	}
	stats, ok := data.([]*entity.PriceChangeStats)
	if !ok {
		fmt.Println("cant convert to slice")
	}

	var priceChange []*wrpc.PriceChangeStats
	for i := range stats {
		priceChange = append(priceChange, convertPriceChangeStats(stats[i]))
	}

	response := wrpc.GetPriceChangeOut{PriceChange: priceChange}

	return &response, nil
}

func (r *Repo) GetDashboard(ctx context.Context, in *wrpc.GetDashboardIn) (*wrpc.DashboardPeriods, error) {
	r.mu.Lock()
	defer r.mu.Unlock()
	data, ok := r.Cache["GetAll"]
	if !ok {
		return nil, errors.New("cant get data from GetDashboard")
	}
	periodsRaw, ok := data.([]*entity.DashboardPeriods)
	if !ok {
		return nil, errors.New("cant convert to map")
	}

	var response wrpc.DashboardPeriods
	for i := range periodsRaw {
		if periodsRaw[i].Symbol == in.Symbol {
			response = *convertDashboardPeriods(periodsRaw[i])
			break
		}
	}

	return &response, nil
}

func convertDashboardPeriods(dashboardPeriods *entity.DashboardPeriods) *wrpc.DashboardPeriods {
	periods := make(map[string]*wrpc.Dashboard, len(dashboardPeriods.Periods))
	for period, dashboard := range dashboardPeriods.Periods {
		var klines []*wrpc.Kline
		for _, kline := range dashboard.Klines {
			klines = append(klines, &wrpc.Kline{
				OpenTime:                 kline.OpenTime,
				Open:                     kline.Open,
				High:                     kline.High,
				Low:                      kline.Low,
				Close:                    kline.Close,
				Volume:                   kline.Volume,
				CloseTime:                kline.CloseTime,
				QuoteAssetVolume:         kline.QuoteAssetVolume,
				TradeNum:                 kline.TradeNum,
				TakerBuyBaseAssetVolume:  kline.TakerBuyBaseAssetVolume,
				TakerBuyQuoteAssetVolume: kline.TakerBuyQuoteAssetVolume,
			})
		}

		periods[period] = &wrpc.Dashboard{
			Symbol: dashboard.Symbol,
			Period: dashboard.Period,
			Market: int32(dashboard.Market),
			Klines: klines,
		}
	}

	return &wrpc.DashboardPeriods{
		Symbol:  dashboardPeriods.Symbol,
		Periods: periods,
	}
}

func convertPriceChangeStats(priceChangeStats *entity.PriceChangeStats) *wrpc.PriceChangeStats {
	return &wrpc.PriceChangeStats{
		Symbol:             priceChangeStats.Symbol,
		PriceChange:        priceChangeStats.PriceChange,
		PriceChangePercent: priceChangeStats.PriceChangePercent,
		WeightedAvgPrice:   priceChangeStats.WeightedAvgPrice,
		PrevClosePrice:     priceChangeStats.PrevClosePrice,
		LastPrice:          priceChangeStats.LastPrice,
		LastQty:            priceChangeStats.LastQty,
		BidPrice:           priceChangeStats.BidPrice,
		BidQty:             priceChangeStats.BidQty,
		AskPrice:           priceChangeStats.AskPrice,
		AskQty:             priceChangeStats.AskQty,
		OpenPrice:          priceChangeStats.OpenPrice,
		HighPrice:          priceChangeStats.HighPrice,
		LowPrice:           priceChangeStats.LowPrice,
		Volume:             priceChangeStats.Volume,
		QuoteVolume:        priceChangeStats.QuoteVolume,
		OpenTime:           priceChangeStats.OpenTime,
		CloseTime:          priceChangeStats.CloseTime,
		FirstId:            priceChangeStats.FirstID,
		LastId:             priceChangeStats.LastID,
		Count:              priceChangeStats.Count,
	}
}
