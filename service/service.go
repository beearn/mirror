package service

import (
	"context"
	pb "gitlab.com/beearn/datawarehouse/wrpc"
	"gitlab.com/beearn/mirror/repo"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Service struct {
	storage repo.Repo
	pb.UnimplementedWarehouseServer
}

func NewService(repo repo.Repo) *Service {
	return &Service{storage: repo}
}

func (s *Service) GetAll() {
	s.storage.GetAllDashboardIn()
	s.storage.GetPriceChangeIn()
}

func (s *Service) GetAllDashboard(ctx context.Context, empty *emptypb.Empty) (*pb.GetAllDashboardOut, error) {
	return s.storage.GetAllDashboard(ctx, empty)
}

func (s *Service) GetDashboardsBySymbols(ctx context.Context, in *pb.GetDashboardsBySymbolsIn) (*pb.GetDashboardsBySymbolsOut, error) {
	return s.storage.GetDashboardsBySymbols(ctx, in)
}
func (s *Service) GetPriceChangeBySymbol(ctx context.Context, in *pb.GetDashboardIn) (*pb.PriceChangeStats, error) {
	return s.storage.GetPriceChangeBySymbol(ctx, in)
}

func (s *Service) GetPriceChange(ctx context.Context, empty *emptypb.Empty) (*pb.GetPriceChangeOut, error) {
	return s.storage.GetPriceChange(ctx, empty)
}
func (s *Service) GetDashboard(ctx context.Context, in *pb.GetDashboardIn) (*pb.DashboardPeriods, error) {
	return s.storage.GetDashboard(ctx, in)
}

func (s *Service) GetSymbols(ctx context.Context, empty *emptypb.Empty) (*pb.GetSymbolsOut, error) {
	all, err := s.GetAllDashboard(ctx, empty)
	if err != nil {
		return nil, err
	}
	var result pb.GetSymbolsOut
	for _, v := range all.Dashboard {
		result.Symbols = append(result.Symbols, v.Symbol)
	}

	return &result, nil
}
